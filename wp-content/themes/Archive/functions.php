<?php

function my_scripts() {
	
	wp_enqueue_script(
		'angularjs',
		get_stylesheet_directory_uri() . '/Users/Haffi/Dropbox/Forritun/Vefforritun/Allaround-Angular/vendor/angular/angular.min.js'
	);

	wp_enqueue_script(
		'angularjs-route',
		get_stylesheet_directory_uri() . '/Users/Haffi/Dropbox/Forritun/Vefforritun/Allaround-Angular/vendor/angular-ui-router/release/angular-ui-router.js'
	);
}

add_action( 'wp_enqueue_scripts', 'my_scripts' );